#ifndef DEBOUNCE
#define DEBOUNCE

#include <Arduino.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Realiza uma leitura para um determinado pino e devolve true caso a leitura corresponda ao estado atual 
 * do botão associado ao pino e a leitura indique que o pino se encontra ativo (estado HIGH). 
 * Em qualquer outra situação a função retorna false, seja por o botão não se encontrar ativo, 
 * seja por a leitura ser considerada ruído (uma vez que o tempo decorrido desde a última leitura considerada válida 
 * não é superior ao intervalo de debouncing);
 */
boolean processDebounce(int pin);


#ifdef __cplusplus
}
#endif

#endif
