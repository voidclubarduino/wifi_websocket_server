/*
 *  This sketch demonstrates how to set up a simple HTTP-like server.
 *  The server will set a GPIO pin depending on the request
 *    http://server_ip/gpio/0 will set the GPIO2 low,
 *    http://server_ip/gpio/1 will set the GPIO2 high
 *  server_ip is the IP address of the ESP8266 module, will be 
 *  printed to Serial when the module is connected.
 */
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include "debounce.h"



#define LED_PIN 2
#define WS_PORT 81
#define HTTP_PORT 82
#define BUTTON_PIN 4

/**
 * WI-FI Configuration
 */
const char* ssid = "";
const char* password = "";

/*
 * LED PIN
 */
int ledState = LOW;

/*
 * Create wifi server on specified port
 */
WiFiServer server(HTTP_PORT);

/*
 * util string based on the websocket port
 */
const String ws_port_str = String(WS_PORT);

/**
 * Create wifi websocket server on specified port
 */
WebSocketsServer webSocket = WebSocketsServer(WS_PORT);


void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(BUTTON_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected to: ");
  Serial.println(ssid);

  /*
   * Start the wifi server
   */
  server.begin();
  Serial.println("Server started");

  /*
   * Start the wifi websocket server
   */
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  Serial.println("WebSocketServer started");

  /* 
   * Print the IP address 
   */
  Serial.println(WiFi.localIP());
}

void loop() {
  processButtonLoop();
  webSocket.loop();
  executeWifiServerLoop();
}


void processButtonLoop(){
  if(processDebounce(BUTTON_PIN)){
    /**
     * Visto a leitura ser válida,
     * Inverter o estado do LED
     */
    ledState = !ledState;
    /**
     * 
     * ativar o pino usando a função:
     *  digitalWrite(PINO[1-13],VALOR[HIGH,LOW])
     *   HIGH = 1 = 5volts --> LIGADO
     *   LOW = 0 = 0volts --> DESLIGADO
     * 
    */
    digitalWrite(LED_PIN, ledState);
    String state = "{\"state\":\"";
    state += String(ledState == 1 ? "on" : "off");
    state += "\"}";
    webSocket.broadcastTXT(state);//broadcast led change
  }
}

/**
 * Process Wifi Server loop
 */
void executeWifiServerLoop(){
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  // Wait until the client sends some data
  while(!client.available()){
    delay(1);
  }
  // Read the first line of the request
  String req = client.readStringUntil('\r');

  String method = req.substring(0, req.indexOf(" "));
  if(method == String('OPTIONS')){
    client.flush();  
    client.print("HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Headers: Access-Control-Allow-Origin\r\nAccess-Control-Allow-Credentials: true\r\nAccess-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEAD\r\nContent-Type: text/html\r\n\r\n");
    return;
  }
  client.flush();
  // Match the request
  int val;
  if (req.indexOf("/state/on") != -1){//turn the led on and send OK response
    writeToLedAndAcknowledge(1, client  );
    return;    
  }else if (req.indexOf("/state/off") != -1){//turn the led off and send OK response
    writeToLedAndAcknowledge(0, client);
    return;
  }else{//return the page
    client.flush();
    String s = getResponseTemplate(client); 
    client.print(s);
  }
}

/**
 * Process webSocketEvents
 */
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        Serial.printf("[%u] Disconnected!\n", num);
        break;
    case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            // send message to client
            webSocket.sendTXT(num, "{\"status\":\"Connected\"}");
        }
        break;
    case WStype_TEXT:
        ledState = strcmp((char*)payload, "on") == 0 ? HIGH : LOW;
        digitalWrite(LED_PIN, ledState);
        String state = "{\"state\":\"";
        state += String((char*)payload);
        state += "\"}";
        webSocket.broadcastTXT(state);//broadcast led change
        break;
  }
}

void writeToLedAndAcknowledge(int val, WiFiClient client){
  ledState = val;
  client.flush(); 
  digitalWrite(LED_PIN, ledState);
  client.print("HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Headers: Access-Control-Allow-Origin\r\nAccess-Control-Allow-Credentials: true\r\nAccess-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEAD\r\nContent-Type: text/html\r\n\r\n");
}

String getResponseTemplate(WiFiClient client){
  String returnValue = (ledState)?"div_arduino_club_on":"div_arduino_club_off";
  String s = getSimpleOKMessageHtmlTemplate();
  s += getHtmlTemplate();
  return s;
}
String getSimpleOKMessageHtmlTemplate(){
  return "HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nAccess-Control-Allow-Headers: Access-Control-Allow-Origin\r\nAccess-Control-Allow-Credentials: true\r\nAccess-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, HEAD\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n";
}

String getHtmlTemplate(){
    String arduinoClubImage=(ledState)?"dac_on":"dac_off";
    String arduinoClubSwitchUp=(ledState)?"dacou":"dacofu";
    String arduinoClubSwitchDown=(ledState)?"dacod":"dacofd";
    String s="<html>";
    s+="<head>";
      s+="<link rel='icon' href='https://i.imgsafe.org/7f0656b.png'>";
      s+="<style>";
      s+="body {" ;
      s+="background-image: url('http://void.pt/css/images/stars2.jpg');";
      s+="}";
      s+="";
      s+="#dacs {";
      s+="margin: 20px auto;";
      s+="width: 202px;";
      s+="height: 120px;";
      s+="}";
      s+="";
      s+=".dacofu {";
      s+="background-image: url('https://i.imgsafe.org/34e31ce.png');";
      s+="width: 103px;";
      s+="height: 120px;";
      s+="cursor: pointer;";
      s+="}";
      s+=".dacofd {";
      s+="background-image: url('https://i.imgsafe.org/345a656.png');";
      s+="width: 99px;";
      s+="height: 120px;";
      s+="}";
      s+=".dacou {";
      s+="background-image: url('https://i.imgsafe.org/3643286.png');";
      s+="width: 96px;";
      s+="height: 120px;";
      s+="}";
      s+=".dacod {";
      s+="background-image: url('https://i.imgsafe.org/35cebe9.png');";
      s+="width: 105px;";
      s+="height: 120px;";
      s+="cursor: pointer;";
      s+="}";
      s+="#sd, #su{";
      s+="float: left;";
      s+="display: inline-block;";
      s+="background-position: center top;";
      s+="background-repeat: no-repeat;";
      s+="}";
      s+=".dac_off {";
      s+="background-image: url('https://i.imgsafe.org/7f0656b.png');";
      s+="}";
      s+="#dac {";
      s+="position: absolute;";
      s+="top: 100px;";
      s+="bottom: 0;";
      s+="background-position: center top;";
      s+="background-repeat: no-repeat;";
      s+="background-size: 360px;";
      s+="width: 100%;";
      s+="}";
      s+="";
      s+=".dac_on {";
      s+="background-image: url('https://i.imgsafe.org/7cf25e5.png');";
      s+="}</style>";
      s+="</head>";
      s+="<body>";
      s+="<div style='position: absolute; bottom: 5%; z-index:1;left: 0; right: 0'>";
      s+="<div id='dacs'>";
      s+="<div id='su' class='"+arduinoClubSwitchUp+"'></div>";
      s+="<div id='sd' class='"+arduinoClubSwitchDown+"'></div>";
      s+="";
      s+="</div>";
      s+="</div>";
      s+="";
      s+="<div id='dac' class='"+arduinoClubImage+"'></div>";
      s+="";
      s += "<script type='text/javascript'>";
      s += "document.getElementById('sd').onclick = function () {";
      s += "if (document.getElementById('sd').className !== 'dacofd') {";
      s += "document.getElementById('su').className = 'dacofu';";
      s += "document.getElementById('sd').className = 'dacofd';";
      s += "if (connection.readyState === 3) {";
      s += "connection = openSocket();";
      s += "}";
      s += "connection.send(\"off\");";
      s += "}";
      s += "};";
      s += "document.getElementById('su').onclick = function () {";
      s += "if (document.getElementById('su').className !== 'dacou') {";
      s += "document.getElementById('su').className = 'dacou';";
      s += "document.getElementById('sd').className = 'dacod';";
      s += "console.log(connection.readyState);";
      s += "if (connection.readyState === 3) {";
      s += "connection = openSocket();";
      s += "}";
      s += "connection.send(\"on\");";
      s += "}";
      s += "};";
      s += "function openSocket() {";
      s += "var connection = new WebSocket('ws://" + toString(WiFi.localIP()) + ":" + ws_port_str + "/echo', ['arduino']);";
      s += "connection.onopen = function () {";
      s += "console.log(\"Connected\");";
      s += "};";
      s += "connection.onerror = function (error) {";
      s += "console.log('WebSocket Error ', error);";
      s += "};";
      s += "connection.onmessage = function (e) {";
      s += "if (e.data) {";
      s += "var stateJSON = JSON.parse(e.data);";
      s += "if (stateJSON.state) {";
      s += "document.getElementById('dac').className = stateJSON.state == 'on' ? 'dac_on' : 'dac_off';";
      s += "document.getElementById('su').className = stateJSON.state == 'on' ? 'dacou' : 'dacofu';";
      s += "document.getElementById('sd').className = stateJSON.state == 'on' ? 'dacod' : 'dacofd';";
      s += "}";
      s += "}";
      s += "};";
      s += "return connection;";
      s += "}";
      s += "var connection = openSocket();";
      s += "</script>";
      s+="</body>";
      s+="</html>";
      return s;
}
/**
 * Convert IP Address to String
 */
String toString(IPAddress address){
    return String(address[0]) + "." + 
        String(address[1]) + "." + 
        String(address[2]) + "." + 
        String(address[3]);
}
