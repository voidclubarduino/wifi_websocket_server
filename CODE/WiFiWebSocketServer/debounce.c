#include <Arduino.h>
#include "debounce.h"
/**
 * Variáveis necessárias para debouncing.
 */
int buttonState;             
int lastButtonState = LOW;   
long lastDebounceTime = 0;
/**
 * DEBOUNCE DELAY
 */
long debounceDelay = 50;

boolean processDebounce(int pin){
  // ler o estado do pino para uma variável local
  int sensorVal = digitalRead(pin);
  /*
   * verificar se o botão foi pressionado e o tempo que decorreu desde o último pressionar do botão 
   * é suficiente para ignorar qualquer tipo de ruído.
  */

  /*
   * Se a leitura registou uma alteração de estado, seja ele ruído ou o pressionar do botão
  */
  if (sensorVal != lastButtonState) {
    // reiniciar o contador de debouncing
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    /**
     * Qualquer que seja a leitura, esta aconteceu a um tempo superior ao intervalo 
     * de debouncing considerado (no exemplo 50 milisegundos).
     * Por essa razão, pode se assumir a leitura como sendo o estado atual do botão.
     */
    if (sensorVal != buttonState) {
      /*
       * o estado atual do botão é diferente ao último estado válido registado, por isso,
       * igualar o último estado válido, para o botão, como sendo a leitura atual.
       */
      buttonState = sensorVal;
      if (buttonState == HIGH) {
        /*
         * definir o último estado lido, para o botão, como sendo a leitura atual.
         */
        lastButtonState = sensorVal;
        /**
          * O botão encontra-se ativo, por isso retorna-se true
        */
        return true;
      }
    }
  }
  /*
   * definir último estado lido, para o botão, como sendo a leitura atual.
   */
  lastButtonState = sensorVal;
  return false;
}
